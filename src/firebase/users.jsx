// users.js
const admin = require('./firebaseAdmin');

const listAllUsers = (nextPageToken) => {
  return admin.auth().listUsers(1000, nextPageToken)
    .then((listUsersResult) => {
      listUsersResult.users.forEach((userRecord) => {
        console.log('user', userRecord.toJSON());
      });
      if (listUsersResult.pageToken) {
        // List next batch of users.
        return listAllUsers(listUsersResult.pageToken);
      }
    })
    .catch((error) => {
      console.log('Error listing users:', error);
    });
};

// Start listing users from the beginning, 1000 at a time.
listAllUsers();