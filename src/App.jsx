import './App.scss'
import { BrowserRouter, Route, Routes, useNavigate } from 'react-router-dom'
// import Sidebar from './components/sidebar/Sidebar'
// import Dashboard from './components/dashboard/Dashboard'
// import Calendar from './components/calendar/Calendar'
// import Employess from './components/employees/Employees'
// import Customers from './components/customers/Customers'
// import { isLoggedIn, removeLoggedIn } from './CookieConfig'
// import Login from './components/auth/login/Login'
// import { useEffect, useState } from 'react'
import { AuthWrapper } from './components/auth/login/AuthWrapper'


function App() {

  return (
    <>
      <BrowserRouter>
      <AuthWrapper />
    </BrowserRouter>
    </>
  )
}

export default App
