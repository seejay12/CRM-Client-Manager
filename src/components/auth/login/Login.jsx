import React, { useState, useEffect } from 'react';
import './login.scss';
import app from '../../../firebase/config'
import { setLoggedIn } from '../../../CookieConfig';
import { getAuth, signInWithEmailAndPassword } from 'firebase/auth';
import { useNavigate } from 'react-router-dom';

const Login = ({ onLogin }) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [passwordVisible, setPasswordVisible] = useState(false);
    const [error, setError] = useState(null);
    const loginNavigate = useNavigate();

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const auth = getAuth(app);
            await signInWithEmailAndPassword(auth, email, password);
            console.log('User logged in successfully');
            setError(null);
            setLoggedIn(); // Ustawiamy informację o zalogowaniu w plikach cookies
            onLogin();
            loginNavigate('/dashboard');
          } catch (error) {
            console.error('Error logging in:', error);
            setError('Niepoprawne dane logowania');
          }
    };
    useEffect(() => {
        // Sprawdzenie, czy użytkownik jest zalogowany przy ponownym załadowaniu strony
        const auth = getAuth(app);
        if (auth.currentUser) {
          onLogin();
        }
      }, [onLogin]);

    const togglePasswordVisibility = () => {
        setPasswordVisible(!passwordVisible);
    };

    return (
        <div className="login-container">
            <div className="login-window">
            <h2>Zaloguj się</h2>
            <form onSubmit={handleSubmit}>
                <div className="form-group">
                    <label htmlFor="email">Email:</label>
                    <input
                        type="email"
                        id="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="password">Hasło:</label>
                    <div className="password-container">
                        <input
                            type={passwordVisible ? 'text' : 'password'}
                            id="password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            required
                        />
                        <span className="toggle-visibility" onClick={togglePasswordVisibility}>
                            {passwordVisible ? '🙈' : '👁️'}
                        </span>
                    </div>
                </div>
                {error && <p style={{ color: 'red' }}>{error}</p>}
                <button type="submit">ZALOGUJ</button>
            </form>
            <div className="signup-link">
                <p>Nie masz konta? <a href="#signup">Zarejestruj się</a></p>
            </div>
            </div>
        </div>
    );
};

export default Login;