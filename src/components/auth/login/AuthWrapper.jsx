import  { useEffect, useState } from 'react';
import { useNavigate, Route, Routes } from 'react-router-dom';
import Sidebar from '../../sidebar/Sidebar';
import Dashboard from '../../dashboard/Dashboard';
import MyCalendar from '../../calendar/Calendar';
import Employees from '../../employees/Employees';
import { isLoggedIn, removeLoggedIn } from '../../../CookieConfig';
import Login from './Login';
import Customers from '../../Customers';
import CustomerDetails from '../../CustomerDetails';
import EmployeeDetails from '../../employees/EmployeeDetails';

export function AuthWrapper() {
  const [loggedIn, setLoggedIn] = useState(isLoggedIn());
  const navigate = useNavigate();

  const handleLogin = () => {
    console.log('User logged in');
    setLoggedIn(true);
  };

  const handleLogout = (e) => {
    e.preventDefault();
    removeLoggedIn(); // Usuwamy informacje o zalogowaniu z cookies
    setLoggedIn(false);
    navigate('/'); // Przekierowujemy na stronę logowania
    window.location.reload(); // Odświeżamy stronę
  };

  useEffect(() => {
    if (loggedIn) {
      navigate('/dashboard');
    } else {
      navigate('/');
    }
  }, []);


  return (
    <>
      {loggedIn && <Sidebar />}
      {loggedIn && <button onClick={handleLogout} className='logout'>Wyloguj</button>}
      <Routes>
        <Route path="/" element={<Login onLogin={handleLogin} />} />
        <Route path="/dashboard" element={<Dashboard />} />
        <Route path="/calendar" element={<MyCalendar />} />
        <Route path="/employees" element={<Employees />} />
        <Route path="/employee/:id" element={<EmployeeDetails />} />
        <Route path="/customers" element={<Customers />} />
        <Route path="/customer/:id" element={<CustomerDetails />} />
      </Routes>
    </>
  );
}
