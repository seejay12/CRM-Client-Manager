import { Link } from 'react-router-dom';
import './sidebar.scss'
import { FaHouse, FaCalendarDays } from "react-icons/fa6";
import { IoMdContacts } from 'react-icons/io';
import { RiContactsLine } from 'react-icons/ri';
import Logo from '../../assets/logo.png'


const Sidebar = () => {
  return (
    <div className="sidebar">
      {/* <Link to="/"><a><FaHouse /> Strona Główna</a></Link> */}
      <Link to="dashboard" className='sidebar-logo'>
          <img src={Logo} />
      </Link>
      <Link to="dashboard" className='sidebar-links'>
          <FaHouse className='sidebar-icon' />Kokpit
      </Link>
      <Link to="calendar" className='sidebar-links'>
          <FaCalendarDays className='sidebar-icon' />Kalendarz
      </Link>
      <Link to="customers" className='sidebar-links'>
          <IoMdContacts className='sidebar-icon' />Klienci
      </Link>
      <Link to="employees" className='sidebar-links'>
          <RiContactsLine className='sidebar-icon' />Pracownicy
      </Link>

    </div>
  );
};

export default Sidebar;
