import React from 'react'
import Login from '../auth/login/Login'

const home = () => {
  const handleLogin = () => {
    console.log('User logged in');
    setLoggedIn(true);
  };
  return (
    <>
    <Login onLogin={handleLogin}/>
    </>
  )
}

export default home