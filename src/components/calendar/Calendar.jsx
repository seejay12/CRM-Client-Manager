import React, { useState, useEffect, useCallback } from 'react';
import { Calendar, momentLocalizer, Views } from 'react-big-calendar';
import moment from 'moment';
import 'moment/locale/pl';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import Modal from 'react-modal';
import { db, auth } from '../../firebase/config';
import { collection, getDocs, addDoc, updateDoc, query, where, doc, deleteDoc } from 'firebase/firestore';
import './Calendar.scss';

moment.locale('pl');
const localizer = momentLocalizer(moment);

Modal.setAppElement('#root');

const formatDateInPolish = (date) => {
  return moment(date).format('D MMMM YYYY[r] HH:mm');
};

const CustomAgendaEvent = ({ event }) => (
  <span>
    <strong>{event.title}</strong>
    {event.client && ` - ${event.client}`}
    {event.description && ` - ${event.description}`}
  </span>
);

const CustomAgendaHeader = ({ label }) => (
  <span>Harmonogram: {label}</span>
);

const MyCalendar = () => {
  const [events, setEvents] = useState([]);
  const [selectedEvent, setSelectedEvent] = useState(null);
  const [isEditing, setIsEditing] = useState(false);
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [client, setClient] = useState('');
  const [clients, setClients] = useState([]);
  const [start, setStart] = useState(new Date());
  const [end, setEnd] = useState(new Date(new Date().getTime() + 60 * 60 * 1000));
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [isAdding, setIsAdding] = useState(false);
  const [view, setView] = useState(Views.MONTH);
  const [date, setDate] = useState(new Date());

  useEffect(() => {
    const fetchClients = async () => {
      try {
        const querySnapshot = await getDocs(collection(db, 'customers'));
        const clientsData = querySnapshot.docs.map(doc => {
          const data = doc.data();
          return {
            id: doc.id,
            firstName: data.firstName,
            lastName: data.lastName,
          };
        });
        setClients(clientsData);
      } catch (error) {
        console.error("Error fetching clients: ", error);
      }
    };

    const fetchEvents = async () => {
      const user = auth.currentUser;
      if (user) {
        const q = query(collection(db, 'events'), where('userId', '==', user.uid));
        const querySnapshot = await getDocs(q);
        const eventsData = querySnapshot.docs.map(doc => ({
          id: doc.id,
          ...doc.data(),
          start: doc.data().start.toDate(),
          end: doc.data().end.toDate(),
        }));
        setEvents(eventsData);
      }
    };

    fetchClients();
    fetchEvents();
  }, []);

  const handleSelectEvent = (event) => {
    setSelectedEvent(event);
    setTitle(event.title);
    setDescription(event.description);
    setClient(event.client);
    setStart(event.start);
    setEnd(event.end);
    setModalIsOpen(true);
    setIsEditing(false);
    setIsAdding(false);
  };

  const handleSave = async () => {
    const user = auth.currentUser;
    if (!user) return;

    if (isAdding) {
      const newEvent = {
        title,
        description,
        client,
        start,
        end,
        userId: user.uid,
      };

      const docRef = await addDoc(collection(db, 'events'), newEvent);
      setEvents((prevEvents) => [
        ...prevEvents,
        { ...newEvent, id: docRef.id },
      ]);
      setIsAdding(false);
    } else {
      const updatedEvent = {
        ...selectedEvent,
        title,
        description,
        client,
        start,
        end,
      };

      await updateDoc(doc(db, 'events', selectedEvent.id), updatedEvent);
      setEvents((prevEvents) =>
        prevEvents.map((evt) =>
          evt.id === selectedEvent.id ? updatedEvent : evt
        )
      );
    }

    setModalIsOpen(false);
    setSelectedEvent(null);
  };

  const handleCancel = () => {
    setModalIsOpen(false);
    setSelectedEvent(null);
    setIsAdding(false);
  };

  const handleEdit = () => {
    setIsEditing(true);
  };

  const handleAddEvent = () => {
    setTitle('');
    setDescription('');
    setClient('');
    setStart(new Date());
    setEnd(new Date(new Date().getTime() + 60 * 60 * 1000));
    setModalIsOpen(true);
    setIsEditing(true);
    setIsAdding(true);
    setSelectedEvent(null);
  };

  const handleDelete = async () => {
    if (!selectedEvent || !selectedEvent.id) return;

    const user = auth.currentUser;
    if (!user) return;

    try {
      await deleteDoc(doc(db, 'events', selectedEvent.id));
      setEvents((prevEvents) => prevEvents.filter((evt) => evt.id !== selectedEvent.id));
      setModalIsOpen(false);
      setSelectedEvent(null);
    } catch (error) {
      console.error("Error deleting event: ", error);
    }
  };

  const handleSelectSlot = (slotInfo) => {
    setSelectedDate(slotInfo.start);
    setStart(slotInfo.start);
    setEnd(new Date(slotInfo.start.getTime() + 60 * 60 * 1000));
    handleAddEvent();
  };

  const handleShowMore = useCallback((events, date) => {
    setDate(date);
    setView(Views.AGENDA);
  }, []);

  const customDayPropGetter = (date) => {
    if (date.getDay() === 0 || date.getDay() === 6) {
      return {
        className: 'weekend-day',
        style: {
          backgroundColor: '#f0f0f0',
        },
      };
    }
    return {};
  };

  const components = {
    agenda: {
      event: CustomAgendaEvent,
      header: CustomAgendaHeader
    }
  };

  const formats = {
    agendaDateFormat: (date, culture, localizer) =>
      localizer.format(date, 'dddd, D MMMM', culture),
    agendaTimeFormat: (date, culture, localizer) =>
      localizer.format(date, 'HH:mm', culture),
    agendaTimeRangeFormat: ({ start, end }, culture, localizer) =>
      localizer.format(start, 'HH:mm', culture) + ' - ' + 
      localizer.format(end, 'HH:mm', culture)
  };

  const messages = {
    allDay: 'Cały dzień',
    previous: 'Poprzedni',
    next: 'Następny',
    today: 'Dzisiaj',
    month: 'Miesiąc',
    week: 'Tydzień',
    day: 'Dzień',
    agenda: 'Agenda',
    date: 'Data',
    time: 'Czas',
    event: 'Wydarzenie',
    noEventsInRange: 'Brak wydarzeń w tym okresie.',
    showMore: (total) => `+${total} więcej`,
  };

  return (
    <div className="calendar-container">
      <button className="btn-add-event" onClick={handleAddEvent}>Dodaj Wydarzenie</button>
      <div className="calendar">
        <Calendar
          localizer={localizer}
          events={events}
          startAccessor="start"
          endAccessor="end"
          onSelectEvent={handleSelectEvent}
          onSelectSlot={handleSelectSlot}
          selectable
          messages={messages}
          formats={{
            monthHeaderFormat: 'MMMM YYYY',
            weekdayFormat: 'dd',
            dayRangeHeaderFormat: ({ start, end }) =>
              `${moment(start).format('D MMMM')} - ${moment(end).format('D MMMM YYYY')}`,
            ...formats
          }}
          views={['month', 'week', 'day', 'agenda']}
          dayPropGetter={customDayPropGetter}
          min={new Date(0, 0, 0, 7, 0, 0)}
          max={new Date(0, 0, 0, 19, 0, 0)}
          className="BigCalendarStyle"
          onShowMore={handleShowMore}
          view={view}
          onView={setView}
          date={date}
          onNavigate={setDate}
          components={components}
        />
      </div>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={handleCancel}
        contentLabel="Szczegóły wydarzenia"
        className="modal"
        overlayClassName="modal-overlay"
      >
        <h3>{isAdding ? 'Dodaj nowe wydarzenie' : 'Szczegóły wydarzenia'}</h3>
        {isEditing || isAdding ? (
          <div>
            <div className="form-group">
              <label>Tytuł:</label>
              <input
                type="text"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label>Opis:</label>
              <textarea
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label>Klient:</label>
              <select
                value={client}
                onChange={(e) => setClient(e.target.value)}
              >
                <option value="">Wybierz klienta</option>
                {clients.map((client) => (
                  <option key={client.id} value={`${client.firstName} ${client.lastName}`}>
                    {client.firstName} {client.lastName}
                  </option>
                ))}
              </select>
            </div>
            <div className="form-group">
              <label>Data rozpoczęcia:</label>
              <input
                type="datetime-local"
                value={moment(start).format('YYYY-MM-DDTHH:mm')}
                onChange={(e) => setStart(new Date(e.target.value))}
              />
            </div>
            <div className="form-group">
              <label>Data zakończenia:</label>
              <input
                type="datetime-local"
                value={moment(end).format('YYYY-MM-DDTHH:mm')}
                onChange={(e) => setEnd(new Date(e.target.value))}
              />
            </div>
            <button className="btn-save" onClick={handleSave}>Zapisz</button>
            <button className="btn-cancel" onClick={handleCancel}>Anuluj</button>
          </div>
        ) : selectedEvent ? (
          <div>
            <p>Tytuł: {selectedEvent.title}</p>
            <p>Opis: {selectedEvent.description}</p>
            <p>Klient: {selectedEvent.client}</p>
            <p>Data rozpoczęcia: {formatDateInPolish(selectedEvent.start)}</p>
            <p>Data zakończenia: {formatDateInPolish(selectedEvent.end)}</p>
            <button className="btn-edit" onClick={handleEdit}>Edytuj</button>
            <button className="btn-delete" onClick={handleDelete}>Usuń</button>
            <button className="btn-cancel" onClick={handleCancel}>Zamknij</button>
          </div>
        ) : null}
      </Modal>
    </div>
  );
};

export default MyCalendar;