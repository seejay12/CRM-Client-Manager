import React, { useEffect, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { db } from '../../firebase/config';
import { doc, getDoc, updateDoc } from 'firebase/firestore';
import { Container, NoteContainer, NoteText, NoteDate, Button, StyledModal, Input, Label, CustomerDetailBox, CustomerNoteBox, CustomerDetailsFlex } from './CustomerDetailsElements';

const CustomerDetails = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const [customer, setCustomer] = useState(null);
  const [editModalIsOpen, setEditModalIsOpen] = useState(false);
  const [noteModalIsOpen, setNoteModalIsOpen] = useState(false);
  const [note, setNote] = useState("");
  const [editCustomer, setEditCustomer] = useState({
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    registrationDate: new Date(),
    city: "",
    status: "",
    additionalInfo: "",
    pesel: "",
    notes: []
  });

  useEffect(() => {
    const fetchCustomer = async () => {
      const customerRef = doc(db, 'customers', id);
      const customerSnap = await getDoc(customerRef);
      if (customerSnap.exists()) {
        const customerData = customerSnap.data();
        setCustomer({
          id: customerSnap.id,
          ...customerData,
          notes: Array.isArray(customerData.notes) ? customerData.notes : [],
          registrationDate: customerData.registrationDate ? new Date(customerData.registrationDate.seconds * 1000) : new Date()
        });
      } else {
        console.error("No such document!");
      }
    };
    fetchCustomer();
  }, [id]);

  const openEditModal = () => {
    if (customer) {
      setEditCustomer({
        ...customer,
        registrationDate: customer.registrationDate ? new Date(customer.registrationDate) : new Date()
      });
      setEditModalIsOpen(true);
    }
  };

  const closeEditModal = () => {
    setEditModalIsOpen(false);
  };

  const handleEditChange = (e) => {
    const { name, value } = e.target;
    const newValue = name === 'registrationDate' ? new Date(value) : value;
    setEditCustomer({ ...editCustomer, [name]: newValue });
  };

  const handleSaveChanges = async () => {
    const customerRef = doc(db, 'customers', editCustomer.id);
    await updateDoc(customerRef, {
      ...editCustomer,
      registrationDate: new Date(editCustomer.registrationDate)
    });

    setCustomer(editCustomer);
    closeEditModal();
  };

  const openNoteModal = () => {
    setNoteModalIsOpen(true);
  };

  const closeNoteModal = () => {
    setNoteModalIsOpen(false);
    setNote("");
  };

  const handleAddNote = async () => {
    if (!note || !customer) return;

    const updatedNotes = [
      {
        text: note,
        date: new Date()
      },
      ...customer.notes
    ];

    const customerRef = doc(db, 'customers', customer.id);
    await updateDoc(customerRef, { notes: updatedNotes });

    setCustomer({ ...customer, notes: updatedNotes });
    closeNoteModal();
  };

  const goBack = () => {
    navigate(-1);
  };

  const formatDate = (date) => {
    if (date instanceof Date) {
      return date.toLocaleString();
    } else if (date.seconds) {
      return new Date(date.seconds * 1000).toLocaleString();
    } else {
      return 'Invalid Date';
    }
  };

  if (!customer) return <div>Loading...</div>;

  return (
    <Container>
      <p>Szczegóły klienta:</p>
      <h1>{customer.firstName} {customer.lastName}</h1>
      <CustomerDetailsFlex>
        <CustomerDetailBox>
          <p><strong>Email:</strong> {customer.email}</p>
          <p><strong>Phone:</strong> {customer.phone}</p>
          <p><strong>Registration Date:</strong> {customer.registrationDate.toLocaleDateString()}</p>
          <p><strong>City:</strong> {customer.city}</p>
          <p><strong>Status:</strong> {customer.status}</p>
          <p><strong>Informacje dodatkowe:</strong> {customer.additionalInfo}</p>
          <p><strong>Pesel:</strong> {customer.pesel}</p>
          <Button onClick={openEditModal}>Edytuj</Button>
          <Button onClick={goBack}>Wstecz</Button>
        </CustomerDetailBox>
        <CustomerNoteBox>
        <Button onClick={openNoteModal}>Dodaj Nową Notatkę</Button>
          <NoteContainer>
            <h2>Notes</h2>
            {customer.notes.map((note, index) => (
              <div key={index}>
                <NoteText>{note.text}</NoteText>
                <NoteDate>{formatDate(note.date)}</NoteDate>
              </div>
            ))}
          </NoteContainer>
        </CustomerNoteBox>
      </CustomerDetailsFlex>

      <StyledModal isOpen={editModalIsOpen} onRequestClose={closeEditModal}>
        <h2>Edytuj Dane Klienta</h2>
        <Label>Imię:</Label>
        <Input type="text" name="firstName" value={editCustomer.firstName} onChange={handleEditChange} />
        <Label>Nazwisko:</Label>
        <Input type="text" name="lastName" value={editCustomer.lastName} onChange={handleEditChange} />
        <Label>Email:</Label>
        <Input type="email" name="email" value={editCustomer.email} onChange={handleEditChange} />
        <Label>Telefon:</Label>
        <Input type="text" name="phone" value={editCustomer.phone} onChange={handleEditChange} />
        <Label>Data Rejestracji:</Label>
        <Input
          type="date"
          name="registrationDate"
          value={editCustomer.registrationDate instanceof Date ? editCustomer.registrationDate.toISOString().split('T')[0] : ""}
          onChange={handleEditChange}
        />
        <Label>Miasto:</Label>
        <Input type="text" name="city" value={editCustomer.city} onChange={handleEditChange} />
        <Label>Status:</Label>
        <Input type="text" name="status" value={editCustomer.status} onChange={handleEditChange} />
        <Label>Informacje dodatkowe:</Label>
        <Input type="text" name="additionalInfo" value={editCustomer.additionalInfo} onChange={handleEditChange} />
        <Label>Pesel:</Label>
        <Input type="text" name="pesel" value={editCustomer.pesel} onChange={handleEditChange} />
        <Button onClick={handleSaveChanges}>Zapisz zmiany</Button>
        <Button onClick={closeEditModal}>Anuluj</Button>
      </StyledModal>

      <StyledModal isOpen={noteModalIsOpen} onRequestClose={closeNoteModal}>
        <h2>Dodaj Nową Notatkę</h2>
        <Label>Notatka:</Label>
        <Input 
          type="text" 
          value={note} 
          onChange={(e) => setNote(e.target.value)} 
          placeholder="Wpisz notatkę" 
        />
        <Button onClick={handleAddNote}>Dodaj Notatkę</Button>
        <Button onClick={closeNoteModal}>Anuluj</Button>
      </StyledModal>
    </Container>
  );
};

export default CustomerDetails;