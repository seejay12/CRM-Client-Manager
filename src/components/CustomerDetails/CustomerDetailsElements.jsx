import styled from 'styled-components';
import Modal from 'react-modal';

export const Container = styled.div`
  padding: 20px;
  width: 100%;
  overflow-y: auto;
`;

export const Button = styled.button`
  background-color: #007BFF;
  color: white;
  padding: 10px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  margin: 5px;
  &:hover {
    background-color: #0056b3;
  }
`;

export const StyledModal = styled(Modal)`
  background-color: white;
  padding: 20px;
  border-radius: 8px;
  max-width: 500px;
  margin: auto;
`;

export const Input = styled.input`
  display: block;
  width: 100%;
  padding: 10px;
  margin-bottom: 10px;
  border: 1px solid #ccc;
  border-radius: 4px;
`;

export const Label = styled.label`
  font-weight: bold;
  margin-bottom: 5px;
  display: block;
`;

export const Table = styled.table`
  width: 100%;
  border-collapse: collapse;
  margin: 20px 0;
`;

export const Th = styled.th`
  background-color: #f2f2f2;
  padding: 10px;
  text-align: left;
  border-bottom: 1px solid #ddd;
`;

export const Td = styled.td`
  padding: 10px;
  border-bottom: 1px solid #ddd;
  &.status-hot-lead span{
    background-color: yellow;
    color: black;
    text-align: center;
    border-radius: 5px;
    padding: 5px 10px;
  }
  &.status-closed span{
    background-color: red;
    color: white;
    text-align: center;
    border-radius: 5px;
    padding: 5px 10px;
  }
  &.status-lead span{
    background-color: green;
    color: white;
    text-align: center;
    border-radius: 5px;
    padding: 5px 10px;
  }
`;

export const Tr = styled.tr`
  &:nth-child(even) {
    background-color: #f9f9f9;
  }
  cursor: pointer;
`;

export const NoteContainer = styled.div`
  margin-top: 10px;
`;

export const NoteText = styled.p`
  background-color: #f2f2f2;
  padding: 10px;
  border-radius: 4px;
  margin-bottom: 5px;
`;

export const NoteDate = styled.small`
  display: block;
  text-align: right;
  color: #888;
`;

export const CustomerDetailsFlex = styled.div`
    display: flex;
    width: 100%;
    gap: 50px;
    align-items: flex-start;
`;

export const CustomerDetailBox = styled.div`
  background-color: #ffffff;
  width: auto;
  padding: 35px;
  min-width: 320px;
`;
export const CustomerNoteBox = styled.div`
  background-color: #ffffff;
  width: 100%;
  padding: 35px;
`;
