import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { db } from '../../firebase/config';
import { collection, getDocs, addDoc, doc, updateDoc, deleteDoc } from 'firebase/firestore';
import { 
  Container, 
  Button, 
  StyledModal, 
  Input, 
  Table, 
  Th, 
  Td, 
  Tr, 
  Label,
  NoteIconContainer,
  Tooltip
} from './CustomersElements';
import Modal from 'react-modal';
import { FaStickyNote } from 'react-icons/fa';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

Modal.setAppElement("#root");

const Customers = () => {
  const [customers, setCustomers] = useState([]);
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [editModalIsOpen, setEditModalIsOpen] = useState(false);
  const [noteModalIsOpen, setNoteModalIsOpen] = useState(false);
  const [newCustomer, setNewCustomer] = useState({
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    registrationDate: new Date(),
    city: "",
    status: "Lead",
    notes: ""
  });
  const [editCustomer, setEditCustomer] = useState({
    id: "",
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    registrationDate: new Date(),
    city: "",
    status: "Lead",
    notes: ""
  });
  const [note, setNote] = useState("");
  const [selectedCustomer, setSelectedCustomer] = useState(null);
  const navigate = useNavigate();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const querySnapshot = await getDocs(collection(db, 'customers'));
        const customersData = querySnapshot.docs.map(doc => {
          const data = doc.data();
          return { 
            id: doc.id, 
            ...data, 
            notes: Array.isArray(data.notes) ? data.notes : [], // Ensure notes is an array
            registrationDate: data.registrationDate ? new Date(data.registrationDate.seconds * 1000) : new Date() 
          };
        });
        setCustomers(customersData);
      } catch (error) {
        console.error("Error fetching customers: ", error);
      }
    };
    fetchData();
  }, []);
  
  const openModal = () => {
    setModalIsOpen(true);
  };

  const closeModal = () => {
    setModalIsOpen(false);
    setEditModalIsOpen(false);
    setNoteModalIsOpen(false);
  };

  const openEditModal = (customer) => {
    setEditCustomer({
      id: customer.id || "",
      firstName: customer.firstName || "",
      lastName: customer.lastName || "",
      email: customer.email || "",
      phone: customer.phone || "",
      registrationDate: customer.registrationDate ? new Date(customer.registrationDate) : new Date(),
      city: customer.city || "",
      status: customer.status || "Lead",
      notes: customer.notes || ""
    });
    setEditModalIsOpen(true);
  };

  const openNoteModal = (customer) => {
    setSelectedCustomer(customer);
    setNoteModalIsOpen(true);
  };

  const closeNoteModal = () => {
    setNoteModalIsOpen(false);
    setSelectedCustomer(null);
    
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setNewCustomer(prevState => ({
      ...prevState,
      [name]: value
    }));
  };
  const handleDateChange = (date) => {
    setNewCustomer(prevState => ({
      ...prevState,
      registrationDate: date
    }));
  };
  const handleEditDateChange = (date) => {
    setEditCustomer(prevState => ({
      ...prevState,
      registrationDate: date
    }));
  };
  const handleEditChange = (e) => {
    const { name, value } = e.target;
    setEditCustomer(prevState => ({
      ...prevState,
      [name]: value
    }));
  };
  const handleNoteChange = (e) => {
    setNote(e.target.value);
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await addDoc(collection(db, "customers"), newCustomer);
      setCustomers(prevCustomers => [...prevCustomers, newCustomer]);
      setNewCustomer({ firstName: "", lastName: "", email: "", phone: "", registrationDate: new Date(), city: "", status: "Lead", notes: "" });
      closeModal();
    } catch (error) {
      console.error("Error adding customer: ", error);
    }
  };
  const handleEditSubmit = async (e) => {
    e.preventDefault();
    try {
      const customerRef = doc(db, "customers", editCustomer.id);
      await updateDoc(customerRef, {
        firstName: editCustomer.firstName,
        lastName: editCustomer.lastName,
        email: editCustomer.email,
        phone: editCustomer.phone,
        registrationDate: editCustomer.registrationDate,
        city: editCustomer.city,
        status: editCustomer.status,
        notes: editCustomer.notes
      });
      setCustomers(prevCustomers => 
        prevCustomers.map(customer => 
          customer.id === editCustomer.id ? editCustomer : customer
        )
      );
      setEditCustomer({ id: "", firstName: "", lastName: "", email: "", phone: "", registrationDate: new Date(), city: "", status: "Lead", notes: "" });
      closeModal();
    } catch (error) {
      console.error("Error updating customer: ", error);
    }
  };
  const handleDelete = async () => {
    try {
      await deleteDoc(doc(db, "customers", editCustomer.id));
      setCustomers(prevCustomers => 
        prevCustomers.filter(customer => customer.id !== editCustomer.id)
      );
      closeModal();
    } catch (error) {
      console.error("Error deleting customer: ", error);
    }
  };
  const handleAddNote = async () => {
    if (!note || !selectedCustomer) return;

    const newNote = {
      text: note,
      date: new Date()
    };

    const updatedNotes = [newNote, ...selectedCustomer.notes];

    const customerRef = doc(db, 'customers', selectedCustomer.id);
    await updateDoc(customerRef, { notes: updatedNotes });

    setCustomers(customers.map(customer => 
      customer.id === selectedCustomer.id ? { ...customer, notes: updatedNotes } : customer
    ));
    
    setNoteModalIsOpen(false);
    setNote("");
    setSelectedCustomer(null);
  };

  const handleCustomerClick = (customerId) => {
    navigate(`/customer/${customerId}`);
  };

  return (
    <Container>
            <Button onClick={openModal}>Dodaj Klienta</Button>
      <StyledModal isOpen={modalIsOpen} onRequestClose={closeModal}>
        <h2>Dodaj Klienta</h2>
        <form onSubmit={handleSubmit}>
          <Label>
            Imię:
            <Input 
              type="text" 
              name="firstName" 
              value={newCustomer.firstName} 
              onChange={handleChange} 
            />
          </Label>
          <Label>
            Nazwisko:
            <Input 
              type="text" 
              name="lastName" 
              value={newCustomer.lastName} 
              onChange={handleChange} 
            />
          </Label>
          <Label>
            Email:
            <Input 
              type="email" 
              name="email" 
              value={newCustomer.email} 
              onChange={handleChange} 
            />
          </Label>
          <Label>
            Nr Telefonu:
            <Input 
              type="text" 
              name="phone" 
              value={newCustomer.phone} 
              onChange={handleChange} 
            />
          </Label>
          <Label>
            Data Zgłoszenia: <br />
            <DatePicker 
              selected={newCustomer.registrationDate} 
              onChange={handleDateChange} 
            />
          </Label> <br />
          <Label>
            Miasto:
            <Input 
              type="text" 
              name="city" 
              value={newCustomer.city} 
              onChange={handleChange} 
            />
          </Label>
          <Label>
            Status: <br />
            <select 
              name="status" 
              value={newCustomer.status} 
              onChange={handleChange}>
              <option value="Hot Lead">Hot Lead</option>
              <option value="Closed">Closed</option>
              <option value="Lead">Lead</option>
            </select>
          </Label> <br /><br />
          <Button type="submit">Dodaj</Button>
        </form>
      </StyledModal>
      <StyledModal isOpen={editModalIsOpen} onRequestClose={closeModal}>
        <h2>Edit Customer</h2>
        <form onSubmit={handleEditSubmit}>
          <Label>
            Imię:
            <Input 
              type="text" 
              name="firstName" 
              value={editCustomer.firstName} 
              onChange={handleEditChange} 
            />
          </Label>
          <Label>
            Nazwisko:
            <Input 
              type="text" 
              name="lastName" 
              value={editCustomer.lastName} 
              onChange={handleEditChange} 
            />
          </Label>
          <Label>
            Email:
            <Input 
              type="email" 
              name="email" 
              value={editCustomer.email} 
              onChange={handleEditChange} 
            />
          </Label>
          <Label>
            Nr Telefonu:
            <Input 
              type="text" 
              name="phone" 
              value={editCustomer.phone} 
              onChange={handleEditChange} 
            />
          </Label>
          <Label>
            Data Zgłoszenia: <br />
            <DatePicker 
              selected={editCustomer.registrationDate} 
              onChange={handleEditDateChange} 
            />
          </Label>
          <Label>
            Miasto:
            <Input 
              type="text" 
              name="city" 
              value={editCustomer.city} 
              onChange={handleEditChange} 
            />
          </Label>
          <Label>
            Status: <br />
            <select 
              name="status" 
              value={editCustomer.status} 
              onChange={handleEditChange}>
              <option value="Hot Lead">Hot Lead</option>
              <option value="Closed">Closed</option>
              <option value="Lead">Lead</option>
            </select>
          </Label>
          <Button type="submit">Zapisz</Button>
          <Button type="button" onClick={handleDelete}>Usuń</Button>
        </form>
      </StyledModal>
      <Table>
        <thead>
          <Tr>
            <Th>Imię i Nazwisko</Th>
            <Th>Email</Th>
            <Th>Telefon</Th>
            <Th>Data Zgłoszenia</Th>
            <Th>Miasto</Th>
            <Th>Status</Th>
            <Th>Notatki</Th>
            <Th>Akcje</Th>
          </Tr>
        </thead>
        <tbody>
          {customers.map(customer => (
            <Tr key={customer.id} onClick={() => handleCustomerClick(customer.id)}>
              <Td>{customer.firstName} {customer.lastName}</Td>
              <Td>{customer.email}</Td>
              <Td>{customer.phone}</Td>
              <Td>{customer.registrationDate.toLocaleDateString()}</Td>
              <Td>{customer.city}</Td>
              <Td className={`status-${customer.status.toLowerCase().replace(' ', '-')}`}><span>{customer.status}</span></Td>
              <Td>
                {customer.notes.length > 0 && (
                  <NoteIconContainer>
                    <FaStickyNote />
                    <Tooltip>{customer.notes[0].text}</Tooltip>
                  </NoteIconContainer>
                )}
              </Td>
              <Td>
              <Button onClick={(e) => { e.stopPropagation();openEditModal(customer); }}>Edytuj</Button>
                <Button onClick={(e) => { e.stopPropagation(); openNoteModal(customer); }}>Dodaj Notatke</Button>
              </Td>
            </Tr>
          ))}
        </tbody>
      </Table>

      <StyledModal isOpen={noteModalIsOpen} onRequestClose={closeNoteModal}>
        <h2>Add Note</h2>
        <Label>Note:</Label>
        <Input 
          type="text" 
          value={note} 
          onChange={(e) => setNote(e.target.value)} 
          placeholder="Enter note" 
        />
        <Button onClick={handleAddNote}>Add Note</Button>
        <Button onClick={closeNoteModal}>Cancel</Button>
      </StyledModal>
    </Container>
  );
};

export default Customers;
