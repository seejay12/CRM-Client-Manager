import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { doc, getDoc } from 'firebase/firestore';
import { db } from '../../firebase/config';


const EmployeeDetails = () => {
    const { id } = useParams();
    const [employee, setEmployee] = useState(null);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);

    useEffect(() => {
        const fetchEmployee = async () => {
            try {
                const docRef = doc(db, 'employees', id);
                const docSnap = await getDoc(docRef);
                if (docSnap.exists()) {
                    setEmployee(docSnap.data());
                } else {
                    setError('Nie znaleziono pracownika');
                }
            } catch (err) {
                setError('Wystąpił błąd podczas pobierania danych');
            } finally {
                setLoading(false);
            }
        };

        fetchEmployee();
    }, [id]);

    if (loading) {
        return <div>Ładowanie...</div>;
    }

    if (error) {
        return <div>{error}</div>;
    }

    return (
        <div className="employee-details-container">
            <h2>Szczegóły Pracownika</h2>
            <p>Imię i Nazwisko: {employee.name}</p>
            <p>Email: {employee.email}</p>
            <p>Telefon: {employee.phone}</p>
            <p>Adres zameldowania: {employee.address}</p>
            <p>Miasto: {employee.city}</p>
            <p>Status: {employee.status}</p>
            <p>PESEL: {employee.pesel}</p>
            <p>Data Zgłoszenia: {employee.date}</p>
            <p>Notatki: {employee.notes}</p>
        </div>
    );
};

export default EmployeeDetails;
