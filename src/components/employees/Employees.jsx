import React, { useState, useEffect } from 'react';
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import { collection, getDocs } from 'firebase/firestore';
import { db } from '../../firebase/config';
import { Link } from 'react-router-dom';
import './Employees.scss';

const Employees = () => {
    const [employees, setEmployees] = useState([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);

    useEffect(() => {
        const fetchEmployees = async () => {
            try {
                const auth = getAuth();
                onAuthStateChanged(auth, async (user) => {
                    if (user) {
                        const querySnapshot = await getDocs(collection(db, 'employees'));
                        const employeesList = querySnapshot.docs.map(doc => ({
                            id: doc.id,
                            ...doc.data()
                        }));
                        setEmployees(employeesList);
                    } else {
                        setError('Nie jesteś zalogowany');
                    }
                });
            } catch (err) {
                setError('Wystąpił błąd podczas pobierania danych');
            } finally {
                setLoading(false);
            }
        };

        fetchEmployees();
    }, []);

    if (loading) {
        return <div>Ładowanie...</div>;
    }

    if (error) {
        return <div>{error}</div>;
    }

    return (
        <div className="employees-container">
            <button className="add-button">Dodaj Klienta</button>
            <table>
                <thead>
                    <tr>
                        <th>Imię i Nazwisko</th>
                        <th>Email</th>
                        <th>Telefon</th>
                        <th>Data Zgłoszenia</th>
                        <th>Miasto</th>
                        <th>Status</th>
                        <th>Notatki</th>
                        <th>Akcje</th>
                    </tr>
                </thead>
                <tbody>
                    {employees.map(employee => (
                        <tr key={employee.id}>
                            <td>
                                <Link to={`/employee/${employee.id}`}>
                                    {employee.name}
                                </Link>
                            </td>
                            <td>{employee.email}</td>
                            <td>{employee.phone}</td>
                            <td>{employee.date}</td>
                            <td>{employee.city}</td>
                            <td><span className="status">{employee.status}</span></td>
                            <td>{employee.notes}</td>
                            <td>
                                <button className="edit-button">Edytuj</button>
                                <button className="note-button">Dodaj Notatkę</button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default Employees;
