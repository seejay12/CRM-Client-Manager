// cookieConfig.js
import Cookies from 'js-cookie';

// Funkcja do ustawiania informacji o zalogowaniu
export const setLoggedIn = () => {
  Cookies.set('loggedIn', 'true', { expires: 0.0208333 }); // Ustawiamy cookies na 30 minut
};

// Funkcja do usuwania informacji o zalogowaniu
export const removeLoggedIn = () => {
  Cookies.remove('loggedIn');
};

// Funkcja do sprawdzania, czy użytkownik jest zalogowany
export const isLoggedIn = () => {
  return Cookies.get('loggedIn') === 'true';
};
