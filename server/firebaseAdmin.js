const admin = require('firebase-admin');
const serviceAccount = require('./punkt-deweloperski-dev-firebase-adminsdk-9l5hj-23a51b2735.json'); // Zaktualizuj ścieżkę do swojego pliku klucza usługi

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

module.exports = admin;