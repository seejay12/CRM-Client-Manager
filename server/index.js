// server/index.js
const express = require('express');
const admin = require('./firebaseAdmin');

const app = express();
const PORT = 3001;

app.get('/users', async (req, res) => {
  try {
    const users = [];
    const listAllUsers = async (nextPageToken) => {
      const result = await admin.auth().listUsers(1000, nextPageToken);
      result.users.forEach((userRecord) => {
        users.push(userRecord.toJSON());
      });
      if (result.pageToken) {
        await listAllUsers(result.pageToken);
      }
    };

    await listAllUsers();
    res.status(200).send(users);
  } catch (error) {
    res.status(500).send('Error listing users: ' + error.message);
  }
});

app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
